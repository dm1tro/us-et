# US-ET keyboard layout #

### How to install on Linux ###

- Download `us` file to Desktop
- `sudo cp ~/us /usr/share/X11/xkb/symbols`
- Add `English (US)` layout

### How to install on Windows ###

- Download `us-et.zip` 
- Extract and run `setup.exe`

P.S. File created with "Microsoft Keyboard Layout Creator 1.4"

### Keyboard layout map ###

`Ä` = `rALT`+`A` | `rALT`+`'`

`Ü` = `rALT`+`U` | `rALT`+`[`

`Õ` = `rALT`+`Q` | `rALT`+`]`

`Ö` = `rALT`+`O` | `rALT`+`;`

`Š` = `rALT`+`S`

`Ž` = `rALT`+`Z`

`©` = `rALT`+`C`

`®` = `rALT`+`R`

`£` = `rALT`+`L`

`€` = `rALT`+`E`

`¥` = `rALT`+`Y`

`µ` = `rALT`+`M`

`¶` = `rALT`+`P`

`№` = `rALT`+`N`

`°` = `rALT`+`0`

`₂` = `rALT`+`2` | `²` = `rALT`+`SHIFT`+`2` 

`₃` = `rALT`+`3` | `³` = `rALT`+`SHIFT`+`3` 

`¼` = `rALT`+`7`

`½` = `rALT`+`8`

`¾` = `rALT`+`9`

`≤` = `rALT`+`,`

`≥` = `rALT`+`.`

`≠` = `rALT`+`=`

`§` = `rALT`+`

`¤` = `rALT`+`~` (`rALT`+`SHIFT`+`)

`¿` = `rALT`+`?` (`rALT`+`SHIFT`+`/`)

`¡` = `rALT`+`!` (`rALT`+`SHIFT`+`1`)

`¬` = `rALT`+`-`

`±` = `rALT`+`+` (`rALT`+`SHIFT`+`=`)

 

### Symbols codes ###

https://www.x.org/releases/X11R7.7/doc/libX11/i18n/compose/en_US.UTF-8.html


